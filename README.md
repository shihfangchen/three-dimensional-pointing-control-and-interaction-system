# Three-dimensional pointing control and interaction system

Three-dimensional pointing control and interaction system

# Building MediaPipe Examples

> git clone https://github.com/google/mediapipe.git

> cd mediapipe

> sudo apt install curl gnupg
> curl -fsSL https://bazel.build/bazel-release.pub.gpg | gpg --dearmor > bazel.gpg
> sudo mv bazel.gpg /etc/apt/trusted.gpg.d/
> echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list

> sudo apt update && sudo apt install bazel

sudo apt update && sudo apt full-upgrade

> sudo apt install bazel-2.0.0

> sudo ln -s /usr/bin/bazel-2.0.0 /usr/bin/bazel
 bazel --version  # 2.0.0

#### Ubuntu 18.04 (LTS) uses OpenJDK 11 by default:
> sudo apt install openjdk-11-jdk

>sudo apt-get install libopencv-core-dev libopencv-highgui-dev \
                       libopencv-calib3d-dev libopencv-features2d-dev \
                       libopencv-imgproc-dev libopencv-video-dev


#### Building Python package from source
> pip3 install -r requirements.txt

> python3.6 setup.py gen_protos

> python3.6 setup.py install --link-opencv

#### Running on GPU

> sed -i 's/capture.open(0)/capture.open(4)/g' mediapipe/examples/desktop/demo_run_graph_main_gpu.cc 
> sed -i 's/cv::waitKey(5)/cv::waitKey(1)/g' mediapipe/examples/desktop/demo_run_graph_main_gpu.cc 

> bazel build -c opt --copt -DMESA_EGL_NO_X11_HEADERS --copt -DEGL_NO_X11 mediapipe/examples/desktop/upper_body_pose_tracking:upper_body_pose_tracking_gpu

> GLOG_logtostderr=1 bazel-bin/mediapipe/examples/desktop/upper_body_pose_tracking/upper_body_pose_tracking_gpu --calculator_graph_config_file=mediapipe/graphs/pose_tracking/upper_body_pose_tracking_gpu.pbtxt

