// License: Apache 2.0. See LICENSE file in root directory.
// Copyright(c) 2017 Intel Corporation. All Rights Reserved.

#include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
#include <librealsense2/rsutil.h>
#include "example.hpp"          // Include short list of convenience functions for rendering
#include "../../wrappers/opencv/cv-helpers.hpp"          // Include short list of convenience functions for rendering

// This example will require several standard data-structures and algorithms:
#define _USE_MATH_DEFINES
#include <math.h>
#include <queue>
#include <unordered_set>
#include <map>
#include <thread>
#include <atomic>
#include <mutex>

#include <opencv2/opencv.hpp>

#include <fstream>

cv::Mat get_ocv_img_from_gl_img(GLuint ogl_texture_id)
{
    glBindTexture(GL_TEXTURE_2D, ogl_texture_id);
    GLenum gl_texture_width, gl_texture_height;

    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, (GLint*)&gl_texture_width);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, (GLint*)&gl_texture_height);

    unsigned char* gl_texture_bytes = (unsigned char*) malloc(sizeof(unsigned char)*gl_texture_width*gl_texture_height*3);
    glGetTexImage(GL_TEXTURE_2D, 0 /* mipmap level */, GL_BGR, GL_UNSIGNED_BYTE, gl_texture_bytes);

    return cv::Mat(gl_texture_height, gl_texture_width, CV_8UC3, gl_texture_bytes);
}

using pixel = std::pair<int, int>;

// Distance 3D is used to calculate real 3D distance between two pixels
float dist_3d(const rs2::depth_frame& frame, pixel u, pixel v);

// Toggle helper class will be used to render the two buttons
// controlling the edges of our ruler
struct toggle
{
    toggle() : x(0.f), y(0.f) {}
    toggle(float xl, float yl)
        : x(std::min(std::max(xl, 0.f), 1.f)),
          y(std::min(std::max(yl, 0.f), 1.f))
    {}

    // Move from [0,1] space to pixel space of specific frame
    pixel get_pixel(rs2::depth_frame frm) const
    {
        int px = x * frm.get_width();
        int py = y * frm.get_height();
        return{ px, py };
    }

    void render(const window& app)
    {
        glColor4f(0.f, 0.0f, 0.0f, 0.2f);
        render_circle(app, 10);
        render_circle(app, 8);
        glColor4f(1.f, 0.9f, 1.0f, 1.f);
        render_circle(app, 6);
    }

    void render_circle(const window& app, float r)
    {
        const float segments = 16;
        glBegin(GL_TRIANGLE_STRIP);
        for (auto i = 0; i <= segments; i++)
        {
            auto t = 2 * M_PI * float(i) / segments;

            glVertex2f(x * app.width() + cos(t) * r,
                y * app.height() + sin(t) * r);

            glVertex2f(x * app.width(),
                y * app.height());
        }
        glEnd();
    }

    // This helper function is used to find the button
    // closest to the mouse cursor
    // Since we are only comparing this distance, sqrt can be safely skipped
    float dist_2d(const toggle& other) const
    {
        return pow(x - other.x, 2) + pow(y - other.y, 2);
    }

    float x;
    float y;
    bool selected = false;

};

// Application state shared between the main-thread and GLFW events
struct state
{
    bool mouse_down = false;
    toggle ruler_start;
    toggle ruler_end;
};

// Helper function to register to UI events
void register_glfw_callbacks(window& app, state& app_state);

// Distance rendering functions:

// Simple distance is the classic pythagorean distance between 3D points
// This distance ignores the topology of the object and can cut both through
// air and through solid
void render_simple_distance(const rs2::depth_frame& depth,
                            const state& s,
                            const window& app);

void draw_line(float x0, float y0, float x1, float y1, int width)
{
    glPushAttrib(GL_ENABLE_BIT);
    glLineStipple(1, 0x00ff);
    glEnable(GL_LINE_STIPPLE);
    glLineWidth(width);
    glBegin(GL_LINE_STRIP);
    glVertex2f(x0, y0);
    glVertex2f(x1, y1);
    glEnd();
    glPopAttrib();


    //std::cout << "\n" << " draw_linex0 " << x0 << " draw_liney0 " << y0 <<"\n";
    //std::cout << "\n" << " draw_linex1 " << x1 << " draw_liney1 " << y1 <<"\n";

}

using namespace cv;
int main(int argc, char * argv[]) try
{


    using namespace cv;
    using namespace rs2;


    //Mat image;
    //image = imread(argv[1],1);   // Read the file
    //imshow( "Display window", image );  
    //waitKey(0);

    // OpenGL textures for the color and depth frames
    texture depth_image, color_image;

    // Colorizer is used to visualize depth data
    rs2::colorizer color_map;
    // Use black to white color map

    color_map.set_option(RS2_OPTION_COLOR_SCHEME, 2.f);
    // Decimation filter reduces the amount of data (while preserving best samples)
    rs2::decimation_filter dec;
    // If the demo is too slow, make sure you run in Release (-DCMAKE_BUILD_TYPE=Release)
    // but you can also increase the following parameter to decimate depth more (reducing quality)
    dec.set_option(RS2_OPTION_FILTER_MAGNITUDE, 2);
    // Define transformations from and to Disparity domain
    rs2::disparity_transform depth2disparity;
    rs2::disparity_transform disparity2depth(false);
    // Define spatial filter (edge-preserving)
    rs2::spatial_filter spat;
    // Enable hole-filling
    // Hole filling is an agressive heuristic and it gets the depth wrong many times
    // However, this demo is not built to handle holes
    // (the shortest-path will always prefer to "cut" through the holes since they have zero 3D distance)
    //spat.set_option(RS2_OPTION_HOLES_FILL, 5); // 5 = fill all the zero pixels
    // Define temporal filter
    rs2::temporal_filter temp;
    // Spatially align all streams to depth viewport
    // We do this because:
    //   a. Usually depth has wider FOV, and we only really need depth for this demo
    //   b. We don't want to introduce new holes
    rs2::align align_to(RS2_STREAM_DEPTH);

    // Declare RealSense pipeline, encapsulating the actual device and sensors
    rs2::pipeline pipe;

    rs2::config cfg;
    //cfg.enable_stream(RS2_STREAM_DEPTH); // Enable default depth
    cfg.enable_stream(RS2_STREAM_DEPTH, 848, 480, RS2_FORMAT_Z16, 30); // 16 bit格式灰階深度影像 30fps
 
    // For the color stream, set format to RGBA
    // To allow blending of the color frame on top of the depth frame

    cfg.enable_stream(RS2_STREAM_COLOR, RS2_FORMAT_RGB8); // BGR888格式彩色影像 30fps

    //cfg.enable_stream(RS2_STREAM_INFRARED, 1, 848, 480, RS2_FORMAT_Y8, 30); // 8 bit格式左紅外線影像 30fps
    //cfg.enable_stream(RS2_STREAM_INFRARED, 2, 848, 480, RS2_FORMAT_Y8, 30); // 8 bit格式右紅外線影像 30fps
    cfg.enable_stream(RS2_STREAM_INFRARED, 848, 480, RS2_FORMAT_Y8, 30);

    auto profile = pipe.start(cfg);

    auto sensor = profile.get_device().first<rs2::depth_sensor>();

    // Set the device to High Accuracy preset of the D400 stereoscopic cameras
    if (sensor && sensor.is<rs2::depth_stereo_sensor>())
    {
        sensor.set_option(RS2_OPTION_VISUAL_PRESET, RS2_RS400_VISUAL_PRESET_DEFAULT );//###


/*
RS2_RS400_VISUAL_PRESET_DEFAULT 	
RS2_RS400_VISUAL_PRESET_HAND 	
*/

    }

    auto stream = profile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();

    // Create a simple OpenGL window for rendering:
    window app(stream.width(), stream.height(), "RealSense Measure Example");

    // Define application state and position the ruler buttons
    state app_state;
    app_state.ruler_start = { 0.45f, 0.5f };
    app_state.ruler_end   = { 0.55f, 0.5f };
    register_glfw_callbacks(app, app_state);


    // After initial post-processing, frames will flow into this queue:
    rs2::frame_queue postprocessed_frames;
    rs2::frame_queue ori_depth;

    // Alive boolean will signal the worker threads to finish-up
    std::atomic_bool alive{ true };

    // Video-processing thread will fetch frames from the camera,
    // apply post-processing and send the result to the main thread for rendering
    // It recieves synchronized (but not spatially aligned) pairs
    // and outputs synchronized and aligned pairs
    std::thread video_processing_thread([&]() {
        while (alive)
        {

            // Fetch frames from the pipeline and send them for processing
            rs2::frameset data;

            if (pipe.poll_for_frames(&data))
            {

                //// Apply color map for visualization of depth
                data = data.apply_filter(color_map);
                ori_depth.enqueue(data);

                // First make the frames spatially aligned
                data = data.apply_filter(align_to);

                // Decimation will reduce the resultion of the depth image,
                // closing small holes and speeding-up the algorithm
                data = data.apply_filter(dec);

                // To make sure far-away objects are filtered proportionally
                // we try to switch to disparity domain
                data = data.apply_filter(depth2disparity);

                // Apply spatial filtering
                data = data.apply_filter(spat);

                // Apply temporal filtering
                data = data.apply_filter(temp);

                // If we are in disparity domain, switch back to depth
                data = data.apply_filter(disparity2depth);



                // Send resulting frames for visualization in the main thread
                postprocessed_frames.enqueue(data);
            }
        }
    });

    bool set_init_frame = 0;
    rs2::frameset current_frameset;
    rs2::frameset current_frameset_ori_depth;
    Mat ini_frame,diff_frame;
    
    while(app) // Application still alive?
    {
        // Fetch the latest available post-processed frameset
        postprocessed_frames.poll_for_frame(&current_frameset);
        ori_depth.poll_for_frame(&current_frameset_ori_depth);
        if (current_frameset)
        {

            Mat depth_color_mat(Size(stream.width(), stream.height()), CV_8UC3, (void*)current_frameset_ori_depth.get_data(), Mat::AUTO_STEP); 

            //Mat depth_color_mat_thres;
            //threshold( depth_color_mat, depth_color_mat_thres, 100, 255, 0 );
            //imshow("depth_color_mat_thres", depth_color_mat_thres);


            Mat depth_color_mat_applyColorMap;
            applyColorMap(depth_color_mat, depth_color_mat_applyColorMap, COLORMAP_JET);

            imshow("depth_color_mat_applyColorMap", depth_color_mat_applyColorMap); // 著虛擬色彩之深度影像

            auto depth = current_frameset.get_depth_frame();
            auto color = current_frameset.get_color_frame();

            //auto irL_frame = current_frameset.get_infrared_frame(1); // 左紅外線灰階影像
            //auto irR_frame = current_frameset.get_infrared_frame(2); // 右紅外線灰階影像
		
            // 建立OpenCV Mat格式之影像(可依需求不一定要全建立)
            
            //Mat irL_image(Size(stream.width(), stream.height()), CV_8UC1, (void*)irL_frame.get_data(), Mat::AUTO_STEP); // 左紅外線灰階影像
            //Mat irR_image(Size(stream.width(), stream.height()), CV_8UC1, (void*)irR_frame.get_data(), Mat::AUTO_STEP); // 右紅外線灰階影像	
            auto ir_frame = current_frameset.first(RS2_STREAM_INFRARED);
            Mat ir(Size(stream.width(), stream.height()), CV_8UC1, (void*)ir_frame.get_data(), Mat::AUTO_STEP);
            imshow("ir_frame", ir);
            //std::cout<<ir;


            //Mat depth_color_mat_inRange;
            //inRange(depth_color_mat, Scalar(100, 100, 100), Scalar(255, 255, 255), depth_color_mat_inRange);
            //imshow("depth_color_mat_inRange", depth_color_mat_inRange);

            //Mat depth_color_mat_inRange_GRAY;
            //cv::cvtColor(depth_color_mat_thres, depth_color_mat_thres_GRAY, COLOR_BGR2GRAY);

            //Mat ir_depth_mask;
            //bitwise_and(ir,depth_color_mat_inRange,ir_depth_mask);
            //imshow("ir_depth_mask", ir_depth_mask);

            //static Mat element = getStructuringElement( 0, Size( 5,5 ), Point( -1, -1 ) );
            //Mat extract_point_mat;
            //morphologyEx( ir_depth_mask, extract_point_mat, MORPH_OPEN, element );
            //absdiff(ir_depth_mask,extract_point_mat,extract_point_mat);
            //imshow("extract_point_mat", extract_point_mat);



            // 以OpenCV函式顯示擷取到影像(可依需求不一定要全顯示)
            //imshow("Left IR Image", irL_image); // 左紅外線灰階影像
            //imshow("Right IR Image", irR_image); // 右紅外線灰階影像

            auto colorized_depth = current_frameset.first(RS2_STREAM_DEPTH, RS2_FORMAT_RGB8);

            //*** OPENCV
            Mat color_image_CV(Size(stream.width(), stream.height()), CV_8UC3, (void*)color.get_data(), Mat::AUTO_STEP); // 彩色影像
            
            Mat color_image_CV_BGR;
            cv::cvtColor(color_image_CV, color_image_CV_BGR, COLOR_RGB2BGR);

            //imshow("OPENCV Color Image", color_image_CV_BGR); // 彩色影像
            imwrite("/home/csf/Desktop/realsense_BGR.bmp", color_image_CV_BGR); // 彩色影像

            waitKey(1);
            //***

            //glEnable(GL_BLEND);
            // Use the Alpha channel for blending
            //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            // First render the colorized depth image
            depth_image.render(colorized_depth, { 0, 0, app.width(), app.height() });

            // Render the color frame (since we have selected RGBA format
            // pixels out of FOV will appear transparent)
            color_image.render(color, { 0, 0, app.width(), app.height() });


            // Render the simple pythagorean distance
            render_simple_distance(depth, app_state, app);

            // Render the ruler
            app_state.ruler_start.render(app);
            app_state.ruler_end.render(app);

            glColor3f(1.f, 1.f, 1.f);
            //glDisable(GL_BLEND);
        }
    }

    // Signal threads to finish and wait until they do
    alive = false;
    video_processing_thread.join();

    return EXIT_SUCCESS;
}
catch (const rs2::error & e)
{
    std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception& e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}

int frame_width, frame_height;

float dist_3d(const rs2::depth_frame& frame, pixel u, pixel v)
{


    auto ruler_b_x=0.0, ruler_b_y=0.0,ruler_e_x=0.0, ruler_e_y=0.0;

    std::ifstream MyReadFile("/home/csf/Desktop/MediaPipe_point.txt");
    std::string myText;
    // Use a while loop together with the getline() function to read the file line by line

    auto line_idx=1;
    while (getline (MyReadFile, myText)) {
    // Output the text from the file

    if(line_idx == 5) ruler_b_x = std::stoi(myText);
    if(line_idx == 6) ruler_b_y = std::stoi(myText);

    if(line_idx == 2) ruler_e_x = std::stoi(myText); //2 11 
    if(line_idx == 3) ruler_e_y = std::stoi(myText) + 10; //3 12

    //std::cout << myText<<"\n";
    line_idx ++;
    }

    // Close the file
    MyReadFile.close();


    float upixel[2]; // From pixel
    float upoint[3]; // From point (in 3D)

    float vpixel[2]; // To pixel
    float vpoint[3]; // To point (in 3D)

    // Copy pixels into the arrays (to match rsutil signatures)
    upixel[0] = u.first;
    upixel[1] = u.second;
    vpixel[0] = v.first;
    vpixel[1] = v.second;

    auto udist_mannual = frame.get_distance(upixel[0], upixel[1]);
    auto vdist_mannual = frame.get_distance(vpixel[0], vpixel[1]);

    std::cout<<"udist_mannual "<<udist_mannual<<"\n";
    std::cout<<"vdist_mannual "<<vdist_mannual<<"\n";

    //std::cout<<"mannual "<<u.first<<" "<<u.second<<" "<<v.first<<" "<<v.second<<"\n";

    std::cout<<ruler_b_x<<" "<<ruler_b_y<<" "<<ruler_e_x<<" "<<ruler_e_y<<"\n";

    
    upixel[0] = int(ruler_b_x/2);
    upixel[1] = int(ruler_b_y/2);
    
    vpixel[0] = int(ruler_e_x/2);
    vpixel[1] = int(ruler_e_y/2);

    draw_line(ruler_b_x,ruler_b_y, ruler_e_x,ruler_e_y,9);

    // Query the frame for distance
    // Note: this can be optimized
    // It is not recommended to issue an API call for each pixel
    // (since the compiler can't inline these)
    // However, in this example it is not one of the bottlenecks
    auto udist = frame.get_distance(upixel[0], upixel[1]);
    auto vdist = frame.get_distance(vpixel[0], vpixel[1]);


    // Deproject from pixel to point in 3D
    rs2_intrinsics intr = frame.get_profile().as<rs2::video_stream_profile>().get_intrinsics(); // Calibration data
    rs2_deproject_pixel_to_point(upoint, &intr, upixel, udist);
    rs2_deproject_pixel_to_point(vpoint, &intr, vpixel, vdist);


    //std::cout <<"\n" << "frame_width " << frame_width <<"\n";
    //std::cout <<"\n" << "frame_height " << frame_height <<"\n";


   float real_x1 = upixel[0]/0.5, norm_x1=real_x1/frame_width,

         real_y1 = upixel[1]/0.5, norm_x2=real_y1/frame_height,

         real_x2 = vpixel[0]/0.5, norm_y1=real_x2/frame_width,

         real_y2 = vpixel[1]/0.5, norm_y2=real_y2/frame_height;

    //std::cout <<"\n" << "norm_x1 " << norm_x1  <<"\n";
    //std::cout <<"\n" << "norm_x2 " << norm_x2 <<"\n";
    //std::cout <<"\n" << "norm_y1 " << norm_y1 <<"\n";
    //std::cout <<"\n" << "norm_y2 " << norm_y2  <<"\n";

    //draw_line(real_x2,real_y2,real_x2+50,real_y2+50,9);

/* 
    //std::cout <<"\n" << "..2d.."<<"\n";
    //std::cout <<"\n" << "upixel[0] " << upixel[0] <<"\n";
    //std::cout <<"\n" << "upixel[1] " << upixel[1] <<"\n";

    //std::cout <<"\n" << "vpixel[0] " << vpixel[0] <<"\n";
    //std::cout <<"\n" << "vpixel[1] " << vpixel[1] <<"\n";
    //std::cout <<"\n" << "..."<<"\n";
*/
    //std::cout <<"\n" << "..3d.."<<"\n";
    //std::cout <<"\n" << "upoint[0] " << upoint[0] <<"\n";
    //std::cout <<"\n" << "upoint[1] " << upoint[1] <<"\n";
    //std::cout <<"\n" << "upoint[2] " << upoint[2] <<"\n";
 

    //std::cout <<"\n" << "vpoint[0] " << vpoint[0] <<"\n";
    //std::cout <<"\n" << "vpoint[1] " << vpoint[1] <<"\n";
    //std::cout <<"\n" << "vpoint[2] " << vpoint[2] <<"\n";
    //std::cout <<"\n" << "..."<<"\n";




    float hyp_dist = 4;

    float tx = vpoint[0]- upoint[0], ty = vpoint[1]- upoint[1], tz = vpoint[2]- upoint[2];

    float inference_x = ((hyp_dist - upoint[2])/tz) * tx + upoint[0];
    float inference_y = ((hyp_dist - upoint[2])/tz) * ty + upoint[1];



    //std::cout <<"\n" << "..inference_xy.."<<"\n";
    //std::cout <<"\n" << "inference_x " << inference_x <<"\n";
    //std::cout <<"\n" << "inference_y " << inference_y <<"\n";
   // //std::cout <<"\n" << "..."<<"\n";


    float upixel_de[2]; // From pixel
    float vpixel_de[2]; // To pixel
    float inference_pixel[3];
    float inference_pixel_de[2];

    inference_pixel[0] = inference_x;
    inference_pixel[1] = inference_y;
    inference_pixel[2] = hyp_dist;

    rs2_project_point_to_pixel(upixel_de, &intr, upoint);
    rs2_project_point_to_pixel(vpixel_de, &intr, vpoint);
    rs2_project_point_to_pixel(inference_pixel_de, &intr, inference_pixel);

    int real_infer_x = inference_pixel_de[0]/0.5, norm_real_infer_x=real_infer_x/frame_width,

          real_infer_y = inference_pixel_de[1]/0.5, norm_real_infer_y=real_infer_x/frame_height;


    draw_line(real_x2,real_y2, real_infer_x,real_infer_y,9);

    std::ofstream myfile;
    myfile.open ("/home/csf/Desktop/realsense_loc.txt");
    myfile << real_x2 << " "<< real_y2 << " " << real_infer_x << " "<< real_infer_y << "\n";  


    //myfile << "real" << "\n" << real_x2 << " "<< real_y2 << "\n";  
    //myfile << "real_infer" << "\n" << real_infer_x << " "<< real_infer_y << "\n";  
    //std::cout <<"real_xy "<< real_x2 << "\n"<< real_y2 << "\n";  
    //std::cout <<"real_infer_xy"<< real_infer_x << "\n"<< real_infer_y << "\n";  

    //std::cout <<"\n" << "..2dde.."<<"\n";
    //std::cout <<"\n" << "upixel_de[0] " << upixel_de[0] <<"\n";
    //std::cout <<"\n" << "upixel_de[1] " << upixel_de[1] <<"\n";

    //std::cout <<"\n" << "vpixe_de[0] " << vpixel_de[0] <<"\n";
    //std::cout <<"\n" << "vpixel_de[1] " << vpixel_de[1] <<"\n";


    //std::cout <<"\n" << "inference_pixel_de[0] " << inference_pixel_de[0] <<"\n";
    //std::cout <<"\n" << "inference_pixel_de[1] " << inference_pixel_de[1] <<"\n";

    //std::cout <<"\n" << "..."<<"\n";



//***

    // Calculate euclidean distance between the two points
    return sqrt(pow(upoint[0] - vpoint[0], 2) +
                pow(upoint[1] - vpoint[1], 2) +
                pow(upoint[2] - vpoint[2], 2));
}



void render_simple_distance(const rs2::depth_frame& depth,
                            const state& s,
                            const window& app)
{
    pixel center;

    //***
    frame_width = app.width();
    frame_height = app.height();
    //***

    glColor4f(0.f, 0.0f, 0.0f, 0.2f);
    draw_line(s.ruler_start.x * app.width(),
        s.ruler_start.y * app.height(),
        s.ruler_end.x   * app.width(),
        s.ruler_end.y   * app.height(), 9);

    glColor4f(0.f, 0.0f, 0.0f, 0.3f);
    draw_line(s.ruler_start.x * app.width(),
        s.ruler_start.y * app.height(),
        s.ruler_end.x   * app.width(),
        s.ruler_end.y   * app.height(), 7);

    glColor4f(1.f, 1.0f, 1.0f, 1.f);
    draw_line(s.ruler_start.x * app.width(),
              s.ruler_start.y * app.height(),
              s.ruler_end.x   * app.width(),
              s.ruler_end.y   * app.height(), 3);

    auto from_pixel = s.ruler_start.get_pixel(depth);
    auto to_pixel =   s.ruler_end.get_pixel(depth);
    float air_dist = dist_3d(depth, from_pixel, to_pixel);

 //td::cout <<"\n" << "app.width()  " << app.width() <<"\n";
 //std::cout <<"\n" << "app.height()  " << app.height() <<"\n";

 /*
 //std::cout <<"\n" << "s.ruler_start.x  " << s.ruler_start.x  <<"\n";
 //std::cout <<"\n" << "s.ruler_start.x * app.width()  " << s.ruler_start.x * app.width() <<"\n";
 //std::cout <<"\n" << "from_pixel.first  " << from_pixel.first <<"\n";

 //std::cout <<"\n" << "s.ruler_start.y  " << s.ruler_start.y  <<"\n";
 //std::cout <<"\n" << "s.ruler_start.y * app.width()  " << s.ruler_start.y * app.width() <<"\n";
 //std::cout <<"\n" << "from_pixel.second  " << from_pixel.second <<"\n";



 //std::cout <<"\n" << "s.ruler_end.x  " << s.ruler_end.x  <<"\n";
 //std::cout <<"\n" << "s.ruler_end.x * app.width()  " << s.ruler_end.x * app.width() <<"\n";
 //std::cout <<"\n" << "to_pixel.first  " << to_pixel.first <<"\n";

 //std::cout <<"\n" << "s.ruler_end.y  " << s.ruler_end.y  <<"\n";
 //std::cout <<"\n" << "s.ruler_end.y * app.width()  " << s.ruler_end.y * app.width() <<"\n";
 //std::cout <<"\n" << "to_pixel.second  " << to_pixel.second <<"\n";
*/


    center.first  = (from_pixel.first + to_pixel.first) / 2;
    center.second = (from_pixel.second + to_pixel.second) / 2;

    std::stringstream ss;
    ss << int(air_dist * 100) << " cm";
    auto str = ss.str();
    auto x = (float(center.first)  / depth.get_width())  * app.width() + 15;
    auto y = (float(center.second) / depth.get_height()) * app.height() + 15;

    auto w = stb_easy_font_width((char*)str.c_str());

    // Draw dark background for the text label
    glColor4f(0.f, 0.f, 0.f, 0.4f);
    glBegin(GL_TRIANGLES);
    glVertex2f(x - 3, y - 10);
    glVertex2f(x + w + 2, y - 10);
    glVertex2f(x + w + 2, y + 2);
    glVertex2f(x + w + 2, y + 2);
    glVertex2f(x - 3, y + 2);
    glVertex2f(x - 3, y - 10);
    glEnd();

    // Draw white text label
    glColor4f(1.f, 1.f, 1.f, 1.f);
    draw_text(x, y, str.c_str());
}

// Implement drag&drop behaviour for the buttons:
void register_glfw_callbacks(window& app, state& app_state)
{
    app.on_left_mouse = [&](bool pressed)
    {
        app_state.mouse_down = pressed;
    };

    app.on_mouse_move = [&](double x, double y)
    {
        toggle cursor{ float(x) / app.width(), float(y) / app.height() };
        std::vector<toggle*> toggles{
            &app_state.ruler_start,
            &app_state.ruler_end };


        if (app_state.mouse_down)
        {
            toggle* best = toggles.front();
            for (auto&& t : toggles)
            {
                if (t->dist_2d(cursor) < best->dist_2d(cursor))
                {
                    best = t;
                }
            }
            best->selected = true;
        }
        else
        {
            for (auto&& t : toggles) t->selected = false;
        }

        for (auto&& t : toggles)
        {
            if (t->selected) *t = cursor;
        }


    };



}

